package springs;

public class Programmer {
	private String name;
	private int salary;
	
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public Programmer(String name,int salary)
	{
		this.name=name;
		this.salary=salary;
	}
public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

public void code()
{
	System.out.println(getName() +" gets salary of " +getSalary() +" thousand");
}
}
