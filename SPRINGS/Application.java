package springs;

import javax.naming.Context;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class Application {
public static void main(String[] args) {
	
	BeanFactory factory = new XmlBeanFactory( new FileSystemResource("my-spring-beans.xml"));
	Programmer programmer = (Programmer) factory.getBean("programmer");
	programmer.code();
}

}

